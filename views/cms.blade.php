{!! Form::open(['route'=>'setting.post']) !!}
<div class="row">
    <div class="col-sm-5">

        <div class="form-group">
            <label for="">نام وبسایت</label>
            {!! Form::text('cms[site]', settings("site"), ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            <label for="">توضیحات وبسایت</label>
            {!! Form::text('cms[description]', settings("description"), ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            <label for="">کلمات کلیدی</label>
            {!! Form::text('cms[meta-keyword]', settings("meta-keyword"), ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            <label for="">توضیحات کلیدی</label>
            {!! Form::text('cms[meta-description]', settings("meta-description"), ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            <label for="">ایمیل</label>
            {!! Form::text('cms[email]', settings("email"), ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            <label for="">تعدا پست ها در صفحه</label>
            {!! Form::text('cms[postPerPage]', settings("postPerPage"), ['class'=>'form-control']) !!}
        </div>
        {!! Form::submit("ویرایش", ['class' => 'btn btn-primary']) !!}


    </div>

    <div class="col-sm-5">
        <div class="form-group">
            <label for="">آدرس</label>
            {!! Form::text('cms[address]', settings("address"), ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="">شماره تماس</label>
            {!! Form::text('cms[phone]', settings("phone"), ['class'=>'form-control']) !!}
        </div>
    </div>
</div>
{!! Form::close() !!}
