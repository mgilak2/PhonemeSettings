@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-cogs"></i>
    تنظیمات
@stop

@section("content")
    <ul class="nav nav-tabs" role="tablist" id="reserve">
        <li class="active"><a href="#settingsNorm" role="tab" data-toggle="tab">تنظیمات عمومی</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="settingsNorm">
            @include("Setting::cms")
        </div>
    </div>
@stop