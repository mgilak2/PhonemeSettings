<?php

use Illuminate\Database\Seeder;

class SettingsDatabaseSeeder extends Seeder
{
    public function run()
    {
        DB::table("settings")
            ->insert(['key' => "cms", "value" => serialize([])]);
    }
}

