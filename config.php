<?php

return array(
    "name"            => "Setting",
    'serviceprovider' => "Setting\\SettingModuleServiceProvider",
    'dir'             => "Setting",
    'level'           => 'init',
    'theme'           => 'Setting',
    'core'            => true,
);