<?php namespace Settings;

use AdminTheme\AdminTheme;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Phoneme\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var Settings
     */
    private $repository;
    /**
     * @var Request
     */
    private $request;

    public function __construct(Settings $repository,
                                Redirector $redirector,
                                AdminTheme $theme,
                                Request $request)
    {
        $this->redirector = $redirector;
        $this->theme = $theme;
        $this->repository = $repository;
        $this->request = $request;
    }

    public function index()
    {
        return $this->theme->baseDashboard("Setting::index");
    }

    public function postSetting()
    {
        $this->repository->insertOrUpdate(serialize($this->request->get("cms", [])));
        return $this->redirector->route('setting.index');
    }
}