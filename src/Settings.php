<?php

namespace Settings;

use Illuminate\Database\Eloquent\Model as Eloqeunt;

class Settings extends Eloqeunt
{
    public $timestamps = false;
    protected $table = 'settings';
    protected $fillable = ["key", "value"];

    public function insertOrUpdate($value)
    {
        if ($this->where("key", "cms")->get()->count() > 0)
            $this->update(["key" => "cms", "value" => $value]);
        else
            $this->create(["key" => "cms", "value" => $value]);
    }
}