<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;

class SettingControllerTest extends TestCase
{
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();
        $this->migrate();
    }

    public function testIndexShouldResponseBackWithOk()
    {
        $this->call("GET", "/dashboard/setting");
        $this->assertResponseOk();
    }

    public function testIndexViewsShouldHaveAllThemeDependencies()
    {
        $this->call("GET", "/dashboard/setting");
        $this->assertViewHas("user");
    }

    public function testPostCmsSettingShouldInsertOrUpdateSettings()
    {
        $this->call("POST", "/dashboard/setting");
        $this->assertRedirectedToRoute('setting.index');
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->rollback();
    }
}