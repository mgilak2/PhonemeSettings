<?php

class SettingsServiceProviderTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->migrate();
    }

    public function testBootShouldDefineRoutes()
    {
        $this->call('GET', '/dashboard/setting');
        $this->assertResponseOk();
    }

    public function testBootShouldDefineViewsBaseFolder()
    {
        $this->ifCallingARouteRespondBackAsOkMeansOurBaseFolderForViewsAreWokring('GET', '/dashboard/setting');
    }

    private function ifCallingARouteRespondBackAsOkMeansOurBaseFolderForViewsAreWokring($method, $route)
    {
        $this->call($method, $route);
        $this->assertResponseOk();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->rollback();
    }
}